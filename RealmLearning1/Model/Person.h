//
//  Person.h
//  RealmLearning1
//
//  Created by Volodymyr Viniarskyi on 3/5/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import <Realm/RLMObject.h>
#import "City.h"

@interface Person : RLMObject
@property NSString *name;
@property NSData *photo;
@property NSInteger age;
@property City *liveIn;
@end
