//
//  ConnectionManager.h
//  RealmLearning1
//
//  Created by Volodymyr Viniarskyi on 3/5/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"
#import "City.h"
@interface ConnectionManager : NSObject {
    RLMResults<Person *> *persons;
    RLMResults<City *> *cities;
}
@property (strong, nonatomic) RLMResults<Person *> *persons;
@property (strong, nonatomic) RLMResults<City *> *cities;
+ (instancetype)shared;
- (void)addPerson:(Person *)person;
- (void)addCity:(City *)city;
- (void)removePerson:(Person *)person;
@end

