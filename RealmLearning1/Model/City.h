//
//  City.h
//  RealmLearning1
//
//  Created by Volodymyr Viniarskyi on 3/5/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import <Realm/Realm.h>

@interface City : RLMObject
@property NSString *name;
@property NSString *state;
@end
