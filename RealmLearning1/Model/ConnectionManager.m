//
//  ConnectionManager.m
//  RealmLearning1
//
//  Created by Volodymyr Viniarskyi on 3/5/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import "ConnectionManager.h"
#import <Realm/RLMResults.h>
@interface ConnectionManager()
@property(strong, nonatomic) RLMRealm *realm;

@end

static ConnectionManager *shared;

@implementation ConnectionManager
@synthesize realm, cities, persons;
+ (instancetype)shared {

    if (!shared) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            shared = [[self alloc] init];
        });
    }
    return shared;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        realm = [RLMRealm defaultRealm];
        [self updateCities];
        [self updatePeoples];
    }
    return self;
}

- (void)updatePeoples {
    persons = [Person allObjects];
}

- (void)updateCities {
    cities = [City allObjects];
}

- (void)addPerson:(Person *)person {
    [realm transactionWithBlock:^{
        [realm addObject:person];
    }];
    [self updatePeoples];
}

- (void)addCity:(City *)city {
    [realm transactionWithBlock:^{
        [realm addObject:city];
    }];
    [self updateCities];
}

- (void)removePerson:(Person *)person {
    NSError *error = nil;
    [realm transactionWithBlock:^{
        [realm deleteObject:person];
    } error:&error];
    NSLog(@"%@", error.localizedDescription);
}

@end
