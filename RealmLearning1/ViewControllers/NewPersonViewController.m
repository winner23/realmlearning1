//
//  NewPersonViewController.m
//  RealmLearning1
//
//  Created by Volodymyr Viniarskyi on 3/5/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import "NewPersonViewController.h"
#import "Person.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "ConnectionManager.h"
@interface NewPersonViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UIButton *cityButton;

@end

@implementation NewPersonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                                                  target:self action:@selector(saveTouched)];
    self.navigationItem.rightBarButtonItem = addButton;
    self.navigationItem.rightBarButtonItem.enabled =NO;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    RAC(self.navigationItem.rightBarButtonItem, enabled) = [[RACSignal combineLatest:@[self.nameTextFiled.rac_textSignal,
                                                                                       self.ageTextField.rac_textSignal]
                                                                              reduce:^id(NSString *name, NSString *age){
                                                                                  return @([name length]> 3 && [age length]>0);
                                                                              }] throttle:1];
}

- (void)saveTouched {
    Person *personCreated = [[Person alloc]init];
    personCreated.name = self.nameTextFiled.text;
    personCreated.photo = nil;
    personCreated.age = [self.ageTextField.text intValue];
    [[ConnectionManager shared] addPerson:personCreated];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
