//
//  ViewController.m
//  RealmLearning1
//
//  Created by Volodymyr Viniarskyi on 3/5/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import "ViewController.h"
#import "ConnectionManager.h"
#import "NewPersonViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITableView *peopleTableView;
@property (strong, nonatomic) ConnectionManager *dataManager;
@end

@implementation ViewController
@synthesize dataManager;

- (void)viewDidLoad {
    [super viewDidLoad];
    dataManager = [ConnectionManager shared];
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addTouched)];
    self.navigationItem.rightBarButtonItem = addButton;
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.peopleTableView reloadData];
}

- (void)addTouched {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NewPersonViewController *newPersonViewController = [storyBoard instantiateViewControllerWithIdentifier:@"NewPersonViewController"];
    [self.navigationController pushViewController:newPersonViewController animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"personCell" forIndexPath:indexPath];
    if (indexPath.row < [dataManager.persons count]) {
        Person *tempPerson = dataManager.persons[indexPath.row];
        cell.textLabel.text = tempPerson.name;
    }
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [dataManager.persons count];
}

- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *delete = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal
                                                                      title:@"Del"
                                                                    handler:^(UITableViewRowAction * _Nonnull action,
                                                                              NSIndexPath * _Nonnull indexPath) {
                                                                        Person *personForDelete = dataManager.persons[indexPath.row];
                                                                        [dataManager removePerson:personForDelete];
                                                                        [self.peopleTableView reloadData];
                                                                    }];
    
    return @[delete];
}

@end
