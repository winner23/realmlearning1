//
//  AppDelegate.h
//  RealmLearning1
//
//  Created by Volodymyr Viniarskyi on 3/5/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

